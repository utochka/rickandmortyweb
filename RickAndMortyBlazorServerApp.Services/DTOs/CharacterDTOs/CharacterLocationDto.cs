﻿using RickAndMortyBlazorApp.Services.Interfaces.DTOs.CharacterDTOs;

namespace RickAndMortyBlazorServerApp.Services.DTOs.CharacterDTOs
{
    public class CharacterLocationDto : ICharacterLocationDto
    {
        public string Name { get; set; }
        public string Url { get; set; }
    }
}
