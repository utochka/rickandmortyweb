﻿using RickAndMortyBlazorApp.Services.Interfaces.DTOs.CharacterDTOs;

namespace RickAndMortyBlazorServerApp.Services.DTOs.CharacterDTOs
{
    public class CharacterListDto : ICharacterListDto
    {
        public InfoDto Info { get; set; }
        public List<CharacterDto> Results { get; set; }
        IInfoDto ICharacterListDto.Info { get => Info; }
        IList<ICharacterDto> ICharacterListDto.Results
        {
            get => Results.Cast<ICharacterDto>().ToList();
        }
    }
}
