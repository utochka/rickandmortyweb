﻿using RickAndMortyBlazorApp.Services.Interfaces.DTOs.CharacterDTOs;

namespace RickAndMortyBlazorServerApp.Services.DTOs.CharacterDTOs
{
    public class OriginDto : IOriginDto
    {
        public string Name { get; set; }
        public string Url { get; set; }
    }
}
