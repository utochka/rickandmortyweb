﻿using RickAndMortyBlazorApp.Services.Interfaces.DTOs.EpisodeDTOs;

namespace RickAndMortyBlazorServerApp.Services.DTOs.LocationDTOs
{
    public class LocationInfoDto : ILocationInfoDto
    {
        public int Count { get; set; }
        public int Pages { get; set; }
        public string Next { get; set; }
        public string Prev { get; set; }
    }
}
