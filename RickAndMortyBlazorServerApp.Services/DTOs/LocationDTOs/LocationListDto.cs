﻿using RickAndMortyBlazorApp.Services.Interfaces.DTOs.EpisodeDTOs;

namespace RickAndMortyBlazorServerApp.Services.DTOs.LocationDTOs
{
    public class LocationListDto : ILocationListDto
    {
        public LocationInfoDto Info { get; set; }
        public IList<LocationDto> Results { get; set; }

        ILocationInfoDto ILocationListDto.Info { get => Info; }

        IList<ILocationDto> ILocationListDto.Results
        {
            get => Results.Cast<ILocationDto>().ToList();
        }
    }
}
