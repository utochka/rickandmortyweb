﻿using RickAndMortyBlazorApp.Services.Interfaces.Models.EpisodeModels;

namespace RickAndMortyBlazorServerApp.Services.Models.EpisodeModels
{
    public class EpisodeModel : IEpisodeModel
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public string AirDate { get; set; }
        public string EpisodeCode { get; set; }
        public IList<string> Characters { get; set; }
        public IList<string> CharacterUrls { get; set; }
    }
}
