﻿namespace RickAndMortyBlazorServerApp.Services.Models.EpisodeModels
{
    public class EpisodeNameModel
    {
        public string Name { get; set; }
        public string Url { get; set; }
    }
}
