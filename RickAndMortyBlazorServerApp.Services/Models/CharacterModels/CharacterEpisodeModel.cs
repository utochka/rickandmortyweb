﻿using RickAndMortyBlazorApp.Services.Interfaces.Models.CharacterModels;

namespace RickAndMortyBlazorServerApp.Services.Models.CharacterModels
{
    public class CharacterEpisodeModel : ICharacterEpisodeModel
    {
        public string Name { get; set; }
        public string Url { get; set; }
    }
}
