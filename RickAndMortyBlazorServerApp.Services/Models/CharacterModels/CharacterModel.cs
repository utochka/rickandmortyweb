﻿using RickAndMortyBlazorApp.Services.Interfaces.Models.CharacterModels;

namespace RickAndMortyBlazorServerApp.Services.Models.CharacterModels
{
    public class CharacterModel : ICharacterModel
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public string Species { get; set; }
        public string Status { get; set; }
        public string Type { get; set; }
        public string Gender { get; set; }
        public OriginModel Origin { get; set; }
        public CharacterLocationModel Location { get; set; }
        public IList<CharacterEpisodeModel> Episode { get; set; }
        public string Image { get; set; }

        ICharacterLocationModel ICharacterModel.Location => Location;

        IOriginModel ICharacterModel.Origin => Origin;

        IList<ICharacterEpisodeModel> ICharacterModel.Episode
        {
            get
            {
                return Episode.Cast<ICharacterEpisodeModel>().ToList();
            }
        }
    }
}
