﻿using RickAndMortyBlazorApp.Services.Interfaces.Models.CharacterModels;

namespace RickAndMortyBlazorServerApp.Services.Models.CharacterModels
{
    public class OriginModel : IOriginModel
    {
        public string Name { get; set; }
        public string Url { get; set; }
    }
}
