﻿using RickAndMortyBlazorApp.Services.Interfaces.Models.CharacterModels;

namespace RickAndMortyBlazorServerApp.Services.Models.CharacterModels
{
    public class InfoModel : IInfoModel
    {
        public int Count { get; set; }
        public int Pages { get; set; }
        public string Next { get; set; }
        public string Prev { get; set; }
    }
}
