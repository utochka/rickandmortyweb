﻿namespace RickAndMortyBlazorServerApp.Services.Models.CharacterModels
{
    public class CharacterNameModel
    {
        public string Name { get; set; }
        public string Url { get; set; }
    }
}
