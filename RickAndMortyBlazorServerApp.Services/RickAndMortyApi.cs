﻿using Newtonsoft.Json;
using RickAndMortyBlazorApp.Services.Interfaces;
using RickAndMortyBlazorApp.Services.Interfaces.DTOs.CharacterDTOs;
using RickAndMortyBlazorApp.Services.Interfaces.DTOs.EpisodeDTOs;
using RickAndMortyBlazorApp.Services.Interfaces.Models.CharacterModels;
using RickAndMortyBlazorApp.Services.Interfaces.Models.EpisodeModels;
using RickAndMortyBlazorApp.Services.Interfaces.Models.LocationModels;
using RickAndMortyBlazorServerApp.Services.DTOs.CharacterDTOs;
using RickAndMortyBlazorServerApp.Services.DTOs.EpisodeDTOs;
using RickAndMortyBlazorServerApp.Services.DTOs.LocationDTOs;
using RickAndMortyBlazorServerApp.Services.Models.CharacterModels;
using RickAndMortyBlazorServerApp.Services.Models.EpisodeModels;
using RickAndMortyBlazorServerApp.Services.Models.LocationModels;

namespace RickAndMortyBlazorServerApp.Services
{
    public class RickAndMortyApi : IRickAndMortyApi
    {
        #region Properties

        private const string BaseUrl = "https://rickandmortyapi.com/api";
        private static string _characterUrl = $"{BaseUrl}/character";
        private static string _episodeUrl = $"{BaseUrl}/episode";
        private static string _locationUrl = $"{BaseUrl}/location";

        private readonly IList<EpisodeNameModel> _episodeNames = new List<EpisodeNameModel>();
        private readonly IList<CharacterNameModel> _characterNames = new List<CharacterNameModel>();

        #endregion

        #region Characters

        public int CountCharactersPages { get; private set; }

        public async Task<List<ICharacterModel>> GetCharactersModelAsync(int pagesCount)
        {
            var list = new List<ICharacterDto>();
            for (var i = 1; i <= pagesCount; ++i)
            {
                var url = $"{_characterUrl}?page={i}";
                var charactersResponse = await GetCharacters(url);
                list.AddRange(charactersResponse);
            }

            await GetEpisodesAsync();

            var characterModels = MapToCharacterModels(list);

            return (List<ICharacterModel>)characterModels;
        }

        public async Task<IList<ICharacterModel>> GetAllPagesCharactersModelAsync(int skipPages)
        {
            await GetEpisodesAsync();

            var list = new List<ICharacterDto>();
            for (var i = skipPages + 1; i <= CountCharactersPages; ++i)
            {
                var url = $"{_characterUrl}?page={i}";
                var charactersResponse = await GetCharacters(url);
                list.AddRange(charactersResponse);
            }

            var characterModels = MapToCharacterModels(list);

            return characterModels;
        }

        private async Task<IList<ICharacterDto>> GetCharacters(string url)
        {
            var response = await GetResponseAsync<CharacterListDto>(url);

            CountCharactersPages = response.Info.Pages;
            var charactersResponse = new List<ICharacterDto>(response.Results);

            foreach (var item in charactersResponse)
            {
                _characterNames.Add(new CharacterNameModel
                {
                    Name = item.Name,
                    Url = item.Url,
                });
            }

            return charactersResponse;
        }

        private IList<ICharacterModel> MapToCharacterModels(IList<ICharacterDto> dto)
        {
            var models = dto.Select(characterDto =>
            {
                var characterModel = new CharacterModel
                {
                    Id = characterDto.Id,
                    Name = characterDto.Name,
                    Species = characterDto.Species,
                    Status = characterDto.Status,
                    Type = characterDto.Type,
                    Gender = characterDto.Gender,
                    Location = new CharacterLocationModel
                    {
                        Name = characterDto.Location.Name,
                        Url = characterDto.Location.Url,
                    },
                    Origin = new OriginModel
                    {
                        Name = characterDto.Origin.Name,
                        Url = characterDto.Origin.Url,
                    },
                    Episode = characterDto.Episode.Select(episodeUrl => new CharacterEpisodeModel
                    {
                        Url = episodeUrl
                    }).ToList(),
                    Image = characterDto.Image
                };

                var matchingEpisodes = _episodeNames
                .Where(model => characterDto.Episode
                .Any(dto => dto == model.Url))
                .ToList();

                characterModel.Episode = matchingEpisodes
                .Select(e => new CharacterEpisodeModel { Url = e.Url, Name = e.Name })
                .ToList();

                return characterModel;
            }).ToList<ICharacterModel>();

            return models;
        }

        #endregion

        #region Episodes

        public async Task<IList<IEpisodeDto>> GetEpisodesAsync()
        {
            var response = await GetResponseAsync<EpisodeListDto>(_episodeUrl);
            var pagesCount = response.Info.Pages;
            var episodesResponse = new List<IEpisodeDto>();

            for (var i = 1; i <= pagesCount; i++)
            {
                var url = $"{_episodeUrl}?page={i}";
                var dtos = await GetResponseAsync<EpisodeListDto>(url);

                foreach (var item in dtos.Results)
                {
                    _episodeNames.Add(new EpisodeNameModel
                    {
                        Name = item.Name,
                        Url = item.Url,
                    });
                }

                episodesResponse.AddRange(dtos.Results);
            }

            return episodesResponse;
        }

        public async Task<IList<IEpisodeModel>> GetEpisodesModelsAsync()
        {
            var episodesResponse = await GetEpisodesAsync();
            IList<IEpisodeModel> episodeModels = MapToEpisodeModels(episodesResponse);

            return episodeModels;
        }

        private IList<IEpisodeModel> MapToEpisodeModels(IList<IEpisodeDto> dto)
        {
            var models = dto.Select(episodeDto =>
            {
                var episodeModel = new EpisodeModel
                {
                    Id = episodeDto.Id,
                    Name = episodeDto.Name,
                    AirDate = episodeDto.Air_Date,
                    EpisodeCode = episodeDto.Episode,
                    CharacterUrls = episodeDto.Characters
                };

                var matchingCharacters = _characterNames
                .Where(model => episodeDto.Characters
                .Any(dto => dto == model.Url))
                .ToList();

                episodeModel.Characters = matchingCharacters
                .Select(ch => ch.Name)
                .ToList();

                return episodeModel;
            }).ToList<IEpisodeModel>();

            return models;
        }

        #endregion

        #region Locations

        public async Task<IList<ILocationDto>> GetLocationsAsync()
        {
            var response = await GetResponseAsync<LocationListDto>(_locationUrl);
            var pagesCount = response.Info.Pages;
            var locationsResponse = new List<ILocationDto>(response.Results);

            for (var i = 2; i <= pagesCount; i++)
            {
                var url = $"{_locationUrl}?page={i}";
                var dtos = await GetResponseAsync<LocationListDto>(url);

                locationsResponse.AddRange(dtos.Results);
            }

            return locationsResponse;
        }

        public async Task<IList<ILocationModel>> GetLocationsModelsAsync()
        {
            var locationsResponse = await GetLocationsAsync();
            IList<ILocationModel> locationModels = MapToLocationModels(locationsResponse);

            return locationModels;
        }

        private IList<ILocationModel> MapToLocationModels(IList<ILocationDto> dto)
        {
            var models = dto.Select(locationDto =>
            {
                var locationModel = new LocationModel
                {
                    Id = locationDto.Id,
                    Name = locationDto.Name,
                    Type = locationDto.Type,
                    Dimension = locationDto.Dimension,
                    ResidentUrls = locationDto.Residents
                };

                var matchingCharacters = _characterNames
                .Where(model => locationDto.Residents
                .Any(dto => dto == model.Url))
                .ToList();

                locationModel.Residents = matchingCharacters
                .Select(rsdnt => rsdnt.Name)
                .ToList();

                return locationModel;
            }).ToList<ILocationModel>();

            return models;
        }

        #endregion

        #region Get Response

        private async Task<T> GetResponseAsync<T>(string url)
            where T : class
        {
            T response = default(T);

            using HttpClient httpClient = new HttpClient();
            HttpResponseMessage responseMessage = null;

            try
            {
                responseMessage = httpClient.GetAsync(url).Result;
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }

            if (responseMessage.IsSuccessStatusCode)
            {
                var json = await responseMessage.Content.ReadAsStringAsync();
                response = JsonConvert.DeserializeObject<T>(json);
            }
            else
            {
                throw new Exception(responseMessage.StatusCode.ToString());
            }

            return response;
        }

        #endregion
    }
}
