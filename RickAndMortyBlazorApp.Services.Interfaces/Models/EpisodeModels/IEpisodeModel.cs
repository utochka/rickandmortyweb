﻿namespace RickAndMortyBlazorApp.Services.Interfaces.Models.EpisodeModels
{
    public interface IEpisodeModel
    {
        int Id { get; }
        string Name { get; }
        string AirDate { get; }
        string EpisodeCode { get; }
        IList<string> Characters { get; }
        IList<string> CharacterUrls { get; }
    }
}
