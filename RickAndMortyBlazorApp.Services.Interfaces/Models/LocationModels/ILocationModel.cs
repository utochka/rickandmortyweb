﻿namespace RickAndMortyBlazorApp.Services.Interfaces.Models.LocationModels
{
    public interface ILocationModel
    {
        int Id { get; }
        string Name { get; }
        string Type { get; }
        string Dimension { get; }
        IList<string> Residents { get; }
        IList<string> ResidentUrls { get; }
    }
}
