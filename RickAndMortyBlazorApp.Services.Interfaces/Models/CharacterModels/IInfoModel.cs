﻿namespace RickAndMortyBlazorApp.Services.Interfaces.Models.CharacterModels
{
    public interface IInfoModel
    {
        int Count { get; }
        int Pages { get; }
        string Next { get; }
        string Prev { get; }
    }
}
