﻿namespace RickAndMortyBlazorApp.Services.Interfaces.Models.CharacterModels
{
    public interface ICharacterEpisodeModel
    {
        string Name { get; }
        string Url { get; }
    }
}