﻿namespace RickAndMortyBlazorApp.Services.Interfaces.Models.CharacterModels
{ 
    public interface ICharacterLocationModel
    {
        string Name { get; }
        string Url { get; }
    }
}
