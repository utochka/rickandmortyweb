﻿namespace RickAndMortyBlazorApp.Services.Interfaces.Models.CharacterModels
{
    public interface IOriginModel
    {
        string Name { get; }
        string Url { get; }
    }
}
