﻿using RickAndMortyBlazorApp.Services.Interfaces.Models.CharacterModels;
using RickAndMortyBlazorApp.Services.Interfaces.Models.EpisodeModels;
using RickAndMortyBlazorApp.Services.Interfaces.Models.LocationModels;

namespace RickAndMortyBlazorApp.Services.Interfaces
{
    public interface IRickAndMortyApi
    {
        Task<List<ICharacterModel>> GetCharactersModelAsync(int pagesCount);
        Task<IList<ICharacterModel>> GetAllPagesCharactersModelAsync(int skipPages);

        Task<IList<IEpisodeModel>> GetEpisodesModelsAsync();
        Task<IList<ILocationModel>> GetLocationsModelsAsync();
    }
}
