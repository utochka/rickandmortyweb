﻿namespace RickAndMortyBlazorApp.Services.Interfaces.DTOs.CharacterDTOs
{
    public interface ICharacterLocationDto
    {
        string Name { get; }
        string Url { get; }
    }
}
