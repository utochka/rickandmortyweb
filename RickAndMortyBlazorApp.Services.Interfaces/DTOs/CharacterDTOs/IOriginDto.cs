﻿namespace RickAndMortyBlazorApp.Services.Interfaces.DTOs.CharacterDTOs
{
    public interface IOriginDto
    {
        string Name { get; }
        string Url { get; }
    }
}
