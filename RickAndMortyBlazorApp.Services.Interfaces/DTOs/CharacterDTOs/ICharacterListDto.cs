﻿namespace RickAndMortyBlazorApp.Services.Interfaces.DTOs.CharacterDTOs
{
    public interface ICharacterListDto
    {
        IInfoDto Info { get; }
        IList<ICharacterDto> Results { get; }
    }
}
