﻿namespace RickAndMortyBlazorApp.Services.Interfaces.DTOs.EpisodeDTOs
{
    public interface ILocationListDto
    {
        ILocationInfoDto Info { get; }
        IList<ILocationDto> Results { get; }
    }
}
