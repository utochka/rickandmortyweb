﻿namespace RickAndMortyBlazorApp.Services.Interfaces.DTOs.EpisodeDTOs
{
    public interface ILocationDto
    {
        int Id { get; }
        string Name { get; }
        string Type { get; }
        string Dimension { get; }
        List<string> Residents { get; }
        string Url { get; }
        DateTime Created { get; }
    }
}
