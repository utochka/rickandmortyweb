﻿namespace RickAndMortyBlazorApp.Services.Interfaces.DTOs.EpisodeDTOs
{ 
    public interface ILocationInfoDto
    {
        int Count { get; }
        int Pages { get; }
        string Next { get; }
        string Prev { get; }
    }
}
