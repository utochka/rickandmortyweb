﻿namespace RickAndMortyBlazorApp.Services.Interfaces.DTOs.EpisodeDTOs
{ 
    public interface IEpisodeInfoDto
    {
        int Count { get; }
        int Pages { get; }
        string Next { get; }
        string Prev { get; }
    }
}
